//
//  MovieViewModel.swift
//  Napo1
//
//  Created by Pedro Ferreira on 9/04/21.
//

import Foundation

class PostViewModel {
    var posts = [Post]()
    var delegate: PostViewProtocol!

    
    init (delegate: PostViewProtocol) {
        self.delegate = delegate
    }
    
    func fetchPost() {
        ApiServiceURLSession.consumeService(baseUrl: ApiServiceURLSession.baseUrl) { response in
            switch response {
            case .success(let data):
                do{
                    let posts = try JSONDecoder().decode([Post].self, from: data)
                    self.setupUnread(posts: posts)
                    self.delegate.getPostSuccessful()
                } catch {
                    //self.delegate.fail(message: "Unknow error")
                    let data = FileHelper.readJsonAsData("Post")
                    if let posts = try? JSONDecoder().decode([Post].self, from: data) {
                        self.setupUnread(posts: posts)
                        self.delegate.getPostSuccessful()
                    }
                }
            case .failure(_):
                //self.delegate.fail(message: failure.localizedDescription)
                let data = FileHelper.readJsonAsData("Post")
                do {
                    let posts = try JSONDecoder().decode([Post].self, from: data)
                    self.setupUnread(posts: posts)
                    self.delegate.getPostSuccessful()
                } catch {
                    print(error)
                }
            }
        }
    }
    
    func setupUnread(posts: [Post]) {
        var posts = posts
        
        
        if let listFavorites = PreferencesManager.getAllObjects, listFavorites.count != 0 {
            for index in 0 ... posts.count - 1 {
                for favorite in  listFavorites {
                    if listFavorites.count != 0 {
                        if favorite.id == posts[index].id {
                            posts[index].isFavorite = true
                            self.posts = posts
                            self.updateFavorite(index: index)
                        }
                    }
                }
                
                posts[index].isRead = index >= 20
            }
        }
        
        
        self.posts = posts
    }
    
    func isRead(index: Int) {
        posts[index].isRead = true
        self.delegate.getPostSuccessful()
    }
    
    func deletePost(index: Int) {
        self.posts.remove(at: index)
    }
    
    func deleteAllPost() {
        self.posts.removeAll()
        self.delegate.getPostSuccessful()
    }
    
    func updateFavorite(index: Int) {
        posts[index].isFavorite = !posts[index].isFavorite
        self.delegate.getPostSuccessful()
    }
}
