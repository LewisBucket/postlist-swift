//
//  ViewController.swift
//  Napo1
//
//  Created by Pedro Ferreira on 9/04/21.
//
import UIKit


class PostViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // insertar tableview
    @IBOutlet weak var tableView: UITableView!
    private var listMovie: [Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //   tableView.estimatedRowHeight = 100
        //  tableView.rowHeight = UItableView
        //fetchFilms2()
        let cell = UINib(nibName: "MovieCell", bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: "MovieCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        ApiServiceURLSession.consumeService(baseUrl: ApiServiceURLSession.baseUrl) { response in
            switch response {
            case .success(let data):
                do{
                    let list = try JSONDecoder().decode([Movie].self, from: data)
                    print(list)
                    self.listMovie = list
                    self.tableView.reloadData()
                } catch {
                    
                }
            case .failure(let failure):
                print(failure)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listMovie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as? MovieCell else {
            return UITableViewCell()
        }
        
        cell.titleMovieLabel.text = "\(indexPath.row). \(self.listMovie[indexPath.row].title)"
        
        let backgroundView = UIView()
        
        if indexPath.row < 20 {
            
            backgroundView.backgroundColor = UIColor.blue
            cell.backgroundColor = .blue
            cell.selectedBackgroundView = backgroundView
        } else {
            cell.backgroundColor = .white
            backgroundView.backgroundColor = UIColor.white
            cell.selectedBackgroundView = backgroundView
        }
        
        return cell
    }
}
