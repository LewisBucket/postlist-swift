//
//  Data.swift
//  Napo1
//
//  Created by Pedro Ferreira on 9/04/21.
//

import Foundation

struct Post: Codable {
    let userID: Int?
    let id: Int
    let title, body: String
    var isRead: Bool? = false
    var isFavorite: Bool = false
    
    static var listPost: [Post] = []

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}
