//
//  ApiServiceURLSessions.swift
//  Napo1
//
//  Created by Pedro Ferreira on 9/04/21.
//

import Foundation

class ApiServiceURLSession {
    
    static let baseUrl = "https://jsonplaceholder.typicode.com/posts"
    
    typealias parameters = [String: Any]
    
    enum HTTPMethod: String {
        case get        = "GET"
        case post       = "POST"
        case connect    = "CONNECT"
        case delete     = "DELETE"
        case put        = "PUT"
    }
    
    enum Result: Error {
        case success(Data)
        case failure(ErrorRequest)
    }
    
    enum ErrorRequest: Error {
        case connectionError(message: String)
        case unknownError(message: String)
        case authorizationError(message: String, Data)
        case serverError(message: String)
        case jsonError(message: String)
    }
    
    static func consumeService(baseUrl:String,
                        method: HTTPMethod = .get,
                        parameters: parameters? = nil,
                        completion: @escaping (Result) -> Void){
        
        let header = ["Content-Type" : "application/json; charset=utf-8"]

        var request = URLRequest(url: URL(string: baseUrl)!)
        request.allHTTPHeaderFields = header
        request.httpMethod = method.rawValue
        
        if let parameters = parameters {
            let parameterData = parameters.reduce("") { (result, param) -> String in
                return result + "&\(param.key)=\(param.value as! String)"
            }.data(using: .utf8)
            request.httpBody = parameterData
        }
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                do {
                    if let error = error {
                        print(error)
                        completion(Result.failure(.connectionError(message: error.localizedDescription)))
                    } else if let data = data, let responseCode = response as? HTTPURLResponse {
                        switch responseCode.statusCode {
                        case 200:
                            completion(Result.success(data))
                        case 400...499:
                            completion(Result.failure(.authorizationError(message: responseCode.description ,data)))
                        case 500...599:
                            completion(Result.failure(.serverError(message: responseCode.description)))
                        default:
                            completion(Result.failure(.unknownError(message: responseCode.description)))
                            break
                        }
                    }
                } catch {
                    
                }
            }
        }.resume()
    }
    
    static func cancel() {
        URLSession.shared.invalidateAndCancel()
    }
}


