//
//  PreferencesHelper.swift
//  Napo1
//
//  Created by Pedro Ferreira on 23/04/21.
//

import Foundation

class PreferencesManager {
    
    static let shared = PreferencesManager()
    
    static var getAllObjects: [Post]? {
          if let objects = UserDefaults.standard.value(forKey: "user_objects") as? Data {
             let decoder = JSONDecoder()
             if let objectsDecoded = try? decoder.decode(Array.self, from: objects) as [Post] {
                return objectsDecoded
             } else {
                return nil
             }
          } else {
             return nil
          }
       }

     static func saveAllObjects(post: [Post]) {
        print(post.count)
          let encoder = JSONEncoder()
          if let encoded = try? encoder.encode(post) {
             UserDefaults.standard.set(encoded, forKey: "user_objects")
          }
     }
    
    static func remomveFavorite(post: Post) {
        if let objects = getAllObjects {
            let obj = objects.filter({ $0.id != post.id})
            PreferencesManager.saveAllObjects(post: obj)
        }
    }
}
