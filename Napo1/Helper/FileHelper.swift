//
//  FileHelper.swift
//  Napo1
//
//  Created by Pedro Ferreira on 22/04/21.
//

import Foundation
class FileHelper {
    
    class func readJsonAsData(_ name: String) -> Data {
        let jsonString: String = readJsonAsString(name)
        
        guard let jsonData = jsonString.data(using: .utf8) else {
            fatalError("Unable to convert UniTestData.json to Data")
        }
        
        return jsonData
        
    }
    
    
    class func readJsonAsString(_ name: String) -> String {
        
        guard let pathString = Bundle.main.path(forResource: name, ofType: "json")
        else {
            fatalError("UnitTestData.json not found")
        }
        guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
            fatalError("Unable to convert UnitTestData.json to String")
            
        }
        return jsonString
    }
    
    
}

