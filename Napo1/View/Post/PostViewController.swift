//
//  ViewController.swift
//  Napo1
//
//  Created by Pedro Ferreira on 9/04/21.
//
import UIKit


protocol PostViewProtocol {    
    func getPostSuccessful()
    
    func fail(message: String)
}

class PostViewController: UIViewController {
    
    // insertar tableview
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var deleteAllRows: UIButton!
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnFavorites: UIButton!
    
    private var viewModel: PostViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.viewModel = PostViewModel(delegate: self)
        
        let cell = UINib(nibName: "PostTableViewCell", bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: "MovieCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        // self.tableView.register(markFavorite.self, forCellReuseIdentifier: "MovieCell")
        
        self.viewModel.fetchPost()
        
        deleteAllRows.layer.cornerRadius = deleteAllRows.frame.width / 2
        deleteAllRows.layer.masksToBounds = true
        
        //refreshButton.layer.shadowColor = UIColor.darkGray.cgColor
        refreshButton.layer.cornerRadius = deleteAllRows.frame.width / 2
      //  refreshButton.layer.masksToBounds = true
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    @IBAction func filterFavoritesList(_ sender: Any) {
        self.viewModel.posts = PreferencesManager.getAllObjects ?? []
        self.viewModel.setupUnread(posts: self.viewModel.posts)
        self.tableView.reloadData()
    }
    
    @IBAction func allFavoritesFilter(_ sender: Any) {
        self.viewModel.fetchPost()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PostDetailViewController {
            if let sender = sender as? Post {
                vc.viewModel.post = sender
            }
        }
    }
    
    
    @IBAction func deleteAllRowsTapped(_ sender: UIButton){
        self.viewModel.deleteAllPost()
    }
    
    
    @IBAction func refreshButton(_ sender: UIButton){
        self.viewModel.fetchPost()
    }
}

extension PostViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as? PostTableViewCell else {
            return UITableViewCell()
        }
        
        cell.title.text = "\(indexPath.row). \(self.viewModel.posts[indexPath.row].title)"
        cell.index = indexPath.row
        cell.delegate = self
        
        cell.icon.tintColor = viewModel.posts[indexPath.row].isFavorite ? .systemYellow : .lightGray
        
        let backgroundView = UIView()
        
        if !(viewModel.posts[indexPath.row].isRead ?? false) {
            backgroundView.backgroundColor = UIColor.systemTeal
            cell.backgroundColor = .systemTeal
            cell.selectedBackgroundView = backgroundView
        } else {
            cell.backgroundColor = .white
            backgroundView.backgroundColor = UIColor.white
            cell.selectedBackgroundView = backgroundView
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.isRead(index: indexPath.row)
        self.addRemoveFavorite(postID: self.viewModel.posts[indexPath.row].id)
        self.performSegue(withIdentifier: "detailVC", sender: viewModel.posts[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
    -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
            self.viewModel.deletePost(index: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            completionHandler(true)
        }
        deleteAction.image = UIImage(systemName: "trash")
        deleteAction.backgroundColor = .systemRed
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        return configuration
    }
}

extension PostViewController: PostViewProtocol {
    
    func getPostSuccessful() {
        self.tableView.reloadData()
    }
    
    func fail(message: String) {
        let dialogMessage = UIAlertController(title: "ops! something is wrong", message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        dialogMessage.addAction(ok)
        
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
}

extension PostViewController: PostTableViewCellProtocol {
    func addRemoveFavorite(postID: Int) {
        if !self.viewModel.posts[postID].isFavorite {
            PreferencesManager.remomveFavorite(post: self.viewModel.posts[postID])
            self.tableView.reloadData()
        } else {
            if var listPost = PreferencesManager.getAllObjects {
                listPost.append(self.viewModel.posts[postID])
                PreferencesManager.saveAllObjects(post: listPost)
            }
        }
        self.tableView.reloadData()
    }
    
    func iconTapped(index: Int) {
        viewModel.updateFavorite(index: index)
    }
}
