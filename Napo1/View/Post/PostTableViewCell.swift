//
//  MovieCell.swift
//  Napo1
//
//  Created by Pedro Ferreira on 9/04/21.
//

import Foundation
import UIKit

protocol PostTableViewCellProtocol {
    func iconTapped(index: Int)
    func addRemoveFavorite(postID: Int)
}

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIButton!
    @IBOutlet weak var title: UILabel!
    
    var index: Int?
    var delegate: PostTableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func favoriteTapped(_ sender: UIButton) {
        guard let index = self.index else {return}
        self.delegate?.iconTapped(index: index)
        self.delegate?.addRemoveFavorite(postID: index)
    }
    
}
