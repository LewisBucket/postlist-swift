//
//  PostDetailViewController.swift
//  Napo1
//
//  Created by Pedro Ferreira on 9/04/21.
//

import UIKit

class PostDetailViewController: UIViewController {
    
    var viewModel = PostDetailViewModel()
    
    @IBOutlet weak var titleLabelView: UILabel!
    @IBOutlet weak var detailLabelView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.titleLabelView.text = viewModel.post?.title
        self.detailLabelView.text = viewModel.post?.body
    }
}
